# Track Your Notes

Track Your Notes is a web application built with Flask that allows users to create, manage, and track notes.

## Overview

Track Your Notes simplifies note management, providing a user-friendly interface to create, edit, and delete notes.

## Features

- Create new notes with titles and descriptions.
- Edit existing notes.
- Delete notes.
- View a list of all notes, sorted by creation date.

## Usage

1. **Accessing the App:**

   The application is hosted and accessible online at [Track Your Notes on GitLab](https://gitlab.com/NashOmar/trackyournotes.git).

2. **Creating Notes:**

   - Click on the "Add Note" button to create a new note.
   - Provide a title and description for your note.

3. **Editing Notes:**

   - Click on the "Edit" button next to a note to make changes.
   - Modify the title and description as needed.

4. **Deleting Notes:**

   - To remove a note, click the "Delete" button.
   - Confirm the action when prompted.

## Contributing

Contributions to this project are welcome! To contribute:

1. Fork the repository.
2. Create a branch for your feature or bug fix.
3. Make your changes and commit them.
4. Create a pull request with a clear description of your changes.

## License

This project is licensed under the MIT License. See the [LICENSE.md](LICENSE.md) file for details.
