import unittest
from app import app, db, Note

class AppTestCase(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
        self.app = app.test_client()
        with app.app_context():
            db.create_all()

    def tearDown(self):
        with app.app_context():
            db.drop_all()

    def test_home_route(self):
        with app.app_context():
            db.session.add(Note(title="This is my u06 test note", description="Description 1"))
            db.session.add(Note(title="This is another test note", description="Description 2"))
            db.session.commit()

        response = self.app.get('/')

        # Print the response data for debugging
        print(response.data.decode())

        self.assertEqual(response.status_code, 200)
        self.assertIn(b'This is my u06 test note', response.data)
        self.assertIn(b'This is another test note', response.data)


    def test_add_note(self):
        # Testing a POST request to add a new note
        data = {
            'title': 'New Test Note',
            'description': 'New Description'
        }
        response = self.app.post('/add-note', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)  # After adding a note, it should redirect to the home page
        self.assertIn(b'New Test Note', response.data)  # Check if the added note title is present in the response

    def test_edit_note(self):
        with app.app_context():
            db.session.add(Note(title="Test Note", description="Original Description"))
            db.session.commit()
        
        #Testing a post request to edit note
        data = {
            'id': 1,
            'title': 'New Edit Note',
            'description': 'New Edit Description'
        }
        
        response = self.app.post('/edit-note', data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'New Edit Note', response.data)

    def test_delete_note(self):
        with app.app_context():
            # Add a test note to the database
            db.session.add(Note(title="Test Note", description="Original Description"))
            db.session.commit()

            # Get the note_id using the query within the application context
            note = Note.query.filter_by(title="Test Note").first()
            note_id = note.id

            # Now you can perform your delete test
            response = self.app.post('/delete-note', data={'id': note_id}, follow_redirects=True)
            self.assertEqual(response.status_code, 200)

            # Ensure that the note has been deleted
            deleted_note = Note.query.get(note_id)
            self.assertIsNone(deleted_note, "The test note was not deleted from the database")


if __name__ == '__main__':
    unittest.main()