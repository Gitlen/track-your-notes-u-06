from datetime import datetime
from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy, query

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///notes.db'
db = SQLAlchemy(app)


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, title, description):
        self.title = title
        self.description = description


@app.route('/')
def home():
    # Get the notes from the database
    notes = Note.query.order_by(Note.created_at.desc()).all()

    # Render a view
    return render_template("home.html", homeIsActive=True, addNoteIsActive=False, notes=notes)


@app.route("/add-note", methods=['GET', 'POST'])
def addNote():
    if request.method == "GET":
        return render_template("add-note.html", homeIsActive=False, addNoteIsActive=True)

    elif request.method == "POST":
        # Get the fields data
        title = request.form['title']
        description = request.form['description']

        # Save the record to the database
        new_note = Note(title=title, description=description)
        db.session.add(new_note)
        db.session.commit()

        # Redirect to home page
        return redirect("/")


@app.route('/edit-note', methods=['GET', 'POST'])
def editNote():
    if request.method == "GET":
        # Get the id of the note to edit
        note_id = request.args.get('id')
        print(note_id)

        # Get the note details from the db
        note = Note.query.filter_by(id=note_id).first()

        # Direct to edit note page
        return render_template('edit-note.html', note=note)

    elif request.method == "POST":
        note_id = request.form['id']
        title = request.form['title']
        description = request.form['description']

        # Attempt to retrieve the note by ID
        note = Note.query.filter_by(id=note_id).first()

        note.title = title
        note.description = description
        db.session.commit()

        # Redirect to home page
        return redirect("/")


@app.route('/delete-note', methods=['POST'])
def deleteNote():
    # Get the id of the note to delete
    note_id = request.form['id']

    # Delete from the database
    Note.query.filter_by(id=note_id).delete()
    db.session.commit()

    # Redirect to home page
    return redirect("/")


if __name__ == "__main__":
    with app.app_context():  # Set up an application context
        db.create_all()  # Create the database tables
    app.run(debug=True)
